<?php


namespace JanRejnowski\SamComplaints\App\Enums;


use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

class StatusType extends Enum implements LocalizedEnum
{
    public const ToFix              = 1;
    public const ToFinished         = 2;
    public const Technology         = 3;
    public const ProductionDirector = 4;
    public const SaleDirector       = 5;
    public const CEO                = 6;
    public const Transport          = 7;
    public const Shipping           = 8;
    public const Finance            = 9;
    public const Rejected           = 10;
    public const Canceled           = 11;
    public const Completed          = 12;

    public static function getLocalizationKey(): string
    {
        return 'sam-complaints::enums.' . static::class;
    }
}
