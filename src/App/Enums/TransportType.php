<?php


namespace JanRejnowski\SamComplaints\App\Enums;


use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

class TransportType extends Enum implements LocalizedEnum
{
    public const Courier            = 1;
    public const SelfPickup         = 2;
    public const Client             = 3;
}
