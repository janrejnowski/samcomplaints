<?php


namespace JanRejnowski\SamComplaints\App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

class ClaimType extends Enum implements LocalizedEnum
{
    public const QuotaDiscount      = 1;
    public const DiscountQty        = 2;
    public const Reprint            = 3;
    public const RemoveDefects      = 4;
    public const CancelOrder        = 5;
    public const ReprintAndDiscount = 6;
    public const NoteWithoutClaims  = 7;
    public const ToDetermine        = 8;
    public const PercentDiscount    = 9;

    public static function getLocalizationKey(): string
    {
        return 'sam-complaints::enums.' . static::class;
    }
}
