<?php


namespace JanRejnowski\SamComplaints\App\Model;


use Illuminate\Database\Eloquent\Model;

class Defect extends Model
{
    protected $fillable = ['complaint_id', 'complaint_type', 'defect_id', 'machine_id', 'user_id', 'description'];

    public function complaints(){
        return $this->morphTo();
    }
}
