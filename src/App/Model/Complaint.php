<?php


namespace JanRejnowski\SamComplaints\App\Model;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Totem\SamCore\App\Repositories\Contracts\HasFilesInterface;
use Totem\SamCore\App\Traits\HasFiles;
use Totem\SamCore\App\Traits\HasUuid;

class Complaint extends Model implements HasFilesInterface
{
    use SoftDeletes,
        HasFiles,
        HasUuid;

    public function path(): string
    {
        return 'complaints/files';
    }

    protected $casts = [
        'total_price' => 'float(10,2)',
        'total_cost' => 'float(10,2)',
        'discount' => 'float(10,2)',
        'estimated_cost' => 'float(10,2)',
        'currency_rate' => 'float(10,2)',
        'percentage' => 'float(10,2)',
        'unit_price' => 'float(10,2)',
        'prime_income' => 'float(10,2)',
        'total_discount' => 'float(10,2)',
        'balance' => 'float(10,2)',
        'run_return' => 'int',
        'claim_id' => 'int',
        'qty_total' => 'int',
        'qty_repair' => 'int',
        'transport_cost' => 'float(10,2)',
    ];

    public function __construct(array $attributes = [])
    {
        $this->addHidden([
            'deleted_at',
            'updated_at'
        ]);

        $this->fillable([
            'user_id',
            'status_id',
            'complaint_number',
            'invoice_number',
            'prime_income',
            'total_discount',
            'balance',
            'order_number',
            'order_name',
            'customer',
            'qty_total',
            'total_price',
            'total_cost',
            'currency',
            'currency_rate',
            'description',
            'claim_id',
            'run_return',
            'discount',
            'percentage',
            'qty_repair',
            'unit_price',
            'estimated_cost',
            'description_cost',

            'qc_approved',

            'transport_cost',
            'transport_method',

            'sd_approved',
            'sd_description',

            'ceo_approved',
            'ceo_description',

            'confirm_return',

            'correction_invoice_number',
            'correction_invoice_value',

            'correction_order_number'
        ]);

        parent::__construct($attributes);
    }

    public function defects()
    {
        return $this->morphTo(Defect::class, 'complaint');
    }
}
