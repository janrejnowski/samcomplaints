<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Illuminate\Support\Facades\DB;

$factory->define(JanRejnowski\SamComplaints\App\Model\Complaint::class, function (Faker $faker) {
    return [
        'user_id' => random_int(DB::table('users')->min('id'), DB::table('users')->max('id')),
        'status_id' => $faker->numberBetween(1,12),
        'complaint_number' => (string)($faker->unique()->randomNumber()),
        'invoice_number' => $faker->unique()->numerify("BB #####"),
        'prime_income' => $faker->randomFloat(2,0,500000),
        'total_discount' => $faker->randomFloat(2,0,50),
        'balance' => $faker->randomFloat(2,0,500000),
        'order_number' => $faker->numerify("AA #####"),
        'order_name' => $faker->text(20),
        'customer' => $faker->word,
        'qty_total' => $faker->numberBetween(1,100000),
        'total_price' => $faker->randomFloat(2, 200, 500000),
        'total_cost' => $faker->randomFloat(2, 200, 500000),
        'currency' => $faker->randomElement($array = array ('PLN', 'EUR', 'GBP', 'NOK', 'SEK', 'DKK')),
        'currency_rate' => $faker->randomFloat(2, 1, 6),
        'description' => $faker->text(200),
        'claim_id' => $faker->numberBetween(1,9),
        'run_return' => $faker->numberBetween(0,1),
        'discount' => $faker->randomFloat(2,0,50000),
        'percentage' => $faker->randomFloat(2,0,10),
        'qty_repair' => $faker->randomNumber(4),
        'unit_price' => $faker->randomFloat(2,0, 40),
        'estimated_cost' => $faker->randomFloat(2,0,100000),
        'description_cost' => $faker->text(200),
        'qc_approved' => $faker->numberBetween(0,1),
        'transport_cost' => $faker->randomFloat(2,0,10000),
        'transport_method' => $faker->numberBetween(1,3),
        'sd_approved' => $faker->numberBetween(0,1),
        'sd_description' => $faker->text(200),
        'ceo_approved' => $faker->numberBetween(0,1),
        'ceo_description' => $faker->text(200),
        'confirm_return' => $faker->numberBetween(0,1),
        'correction_invoice_number' => $faker->numerify("CC #####"),
        'correction_invoice_value' => $faker->randomFloat(2,0,10000),
        'correction_order_number' => $faker->numerify("DD #####")
    ];
});
