<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;
use Rudashi\Countries\Enums\CurrencyType;

class CreateComplaintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            Schema::create('complaints', function (Blueprint $table) {
                //Salesman
                $table->increments('id')->unsigned();
                $table->timestamps();
                $table->softDeletes();
                $table->uuid('uuid')->unique();
                $table->integer('user_id')->unsigned();
                $table->integer('status_id');
                $table->string('complaint_number')->unique();
                $table->string('invoice_number')->unique();
                $table->decimal('prime_income', 10)->default('0.00');
                $table->decimal('total_discount', 10)->default('0.00');
                $table->decimal('balance', 10)->default('0.00');
                $table->string('order_number');
                $table->string('order_name');
                $table->string('customer');
                $table->integer('qty_total');
                $table->decimal('total_price', 10);
                $table->decimal('total_cost', 10);
                $table->string('currency')->default(CurrencyType::getKey(CurrencyType::PLN));
                $table->decimal('currency_rate', 10)->default('1.00');
                $table->longText('description');
                $table->integer('claim_id');
                $table->tinyInteger('run_return')->default(0);
                $table->decimal('discount', 10)->default('0.00');
                $table->decimal('percentage', 10)->default('0.00');
                $table->integer('qty_repair')->default('0');
                $table->decimal('unit_price', 10)->default('0.00');
                $table->decimal('estimated_cost', 10)->default('0.00');
                $table->longText('description_cost')->nullable();

                //Quality Control
                $table->tinyInteger('qc_approved')->nullable();

                //Transport
                $table->decimal('transport_cost', 10)->nullable();
                $table->integer('transport_method')->nullable();

                //Sales director
                $table->tinyInteger('sd_approved')->nullable();
                $table->longText('sd_description')->nullable();

                //CEO
                $table->tinyInteger('ceo_approved')->nullable();
                $table->longText("ceo_description")->nullable();

                //Shipping
                $table->tinyInteger('confirm_return')->nullable();

                //Finance
                $table->string('correction_invoice_number')->nullable();
                $table->decimal('correction_invoice_value', 10)->nullable();

                //Salesman
                $table->string('correction_order_number')->nullable();

                $table->foreign('user_id')->references('id')->on('users')
                    ->onUpdate('no action')->onDelete('no action');
            });
        } catch (PDOException $ex) {
            $this->down();
            throw $ex;
        }

        Artisan::call('db:seed', [
            '--class' => \JanRejnowski\SamComplaints\Database\Seeds\ComplaintsTableSeeder::class
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('complaints');
    }
}
