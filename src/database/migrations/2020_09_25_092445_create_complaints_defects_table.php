<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComplaintsDefectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            Schema::create('complaints_defects', function (Blueprint $table) {
                $table->increments('id')->unsigned();
                $table->timestamps();
                $table->softDeletes();
                $table->integer('defect_id')->unsigned();
                $table->morphs('complaint');
                $table->integer('machine_id')->unsigned();
                $table->integer('user_id')->unsigned();
                $table->longText('description');

                $table->foreign('user_id')->references('id')->on('users')
                    ->onUpdate('no action')->onDelete('no action');

                $table->foreign('defect_id')->references('id')->on('defects')
                    ->onUpdate('no action')->onDelete('no action');

                $table->foreign('machine_id')->references('id')->on('machines')
                    ->onUpdate('no action')->onDelete('no action');
            });
        }catch (PDOException $ex) {
            $this->down();
            throw $ex;
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('complaints_defects');
    }
}
