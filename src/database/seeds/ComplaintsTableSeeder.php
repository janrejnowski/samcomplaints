<?php

namespace JanRejnowski\SamComplaints\Database\Seeds;

use Illuminate\Database\Seeder;
use JanRejnowski\SamComplaints\App\Model\Complaint;

class ComplaintsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Complaint::class, 200) -> create();
    }
}
