<?php

namespace JanRejnowski\SamComplaints;

use Illuminate\Support\ServiceProvider;
use Totem\SamCore\App\Traits\TraitServiceProvider;

class SamComplaintsServiceProvider extends ServiceProvider
{
    use TraitServiceProvider;

    public function getNamespace() : string
    {
        return 'sam-complaints';
    }

    public function boot(): void
    {
        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');
    }

    public function register(): void
    {
        $this->registerEloquentFactoriesFrom(__DIR__. '/database/factories');
    }
}
